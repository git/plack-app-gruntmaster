let offset = 0;

function update_timer(timer){
	const start = parseInt(timer.dataset.start);
	const stop  = parseInt(timer.dataset.stop);
	const value = parseInt(timer.dataset.value);
	const now   = Math.floor(Date.now() / 1000) + offset;

	const left = Math.max(stop - now, 0);
	const total = stop - start;

	if(value)
		timer.innerHTML = Math.floor(Math.max(value * 3 / 10, value * left / total));
	else {
		let hours = Math.floor(left / 60 / 60);
		hours = hours < 10 ? '0' + hours : hours;
		let minutes = Math.floor(left / 60) % 60;
		minutes = minutes < 10 ? '0' + minutes : minutes;
		let seconds = left % 60;
		seconds = seconds < 10 ? '0' + seconds : seconds;
		timer.innerHTML = hours + ':' + minutes + ':' + seconds;
	}
}

$(() => {
	$('.timer').each(item => {
		update_timer(item);
		setInterval(() => update_timer(item), 1000);
	});
	if($('.timer').length > 0) {
		const xhr = new XMLHttpRequest();
		xhr.open('HEAD', '/');
		xhr.onload = () => {
			const srvtime = Date.parse(xhr.getResponseHeader('Date'));
			offset = Math.ceil((srvtime - Date.now()) / 1000);
			console.log("Offset from server: " + offset);
		};
		xhr.send();
	}
});
