function m(html) {
	const div = document.createElement("div");
	div.innerHTML = html;
	return div.firstChild;
}

function q(selector) {
	return document.querySelector(selector);
}
