let modal, hiding;

function show_modal () {
	modal.classList.remove('hidden');
	q('.backdrop').classList.remove('hidden');
	setTimeout(function (){
		document.body.classList.add('modal-open');
	}, 20);
}

function hide_modal () {
	document.body.classList.remove('modal-open');
}

$(function() {
	modal = q('.modal');
	$(modal).on('transitionend', el => {
		if(!document.body.classList.contains('modal-open'))
			modal.classList.add('hidden');
	});
	$('.backdrop,.modal').each(el => document.body.appendChild(el) );
	$('#solution').on('click', e => {
		if(window.matchMedia("(min-width: 768px)").matches){
			show_modal();
			e.preventDefault();
		}
	});
	$('.backdrop').on('click', hide_modal);
	window.onkeyup = event => {
		if (event.keyCode === 27)
			hide_modal();
	}
});
