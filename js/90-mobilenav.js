$(function(){
	const nav = q('nav');
	nav.classList.add('hidden-xs');
	q('#title').insertAdjacentHTML('beforebegin', '<div class="text-center visible-xs-block">Tap title to toggle menu</div>');
	$('#title').on('click', () => nav.classList.toggle('hidden-xs'));
});
