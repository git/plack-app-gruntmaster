use 5.014000;
use ExtUtils::MakeMaker;

WriteMakefile(
	NAME              => 'Plack::App::Gruntmaster',
	VERSION_FROM      => 'lib/Plack/App/Gruntmaster.pm',
	ABSTRACT_FROM     => 'lib/Plack/App/Gruntmaster.pm',
	AUTHOR            => 'Marius Gavrilescu <marius@ieval.ro>',
	MIN_PERL_VERSION  => '5.14.0',
	LICENSE           => 'AGPL_3',
	SIGN              => 1,
	clean             => {
		FILES => 'static/css/ static/js/'
	},
	BUILD_REQUIRES    => {
		qw/CSS::Minifier::XS          0
		   File::Slurp                0
		   Test::More                 0
		   Test::WWW::Mechanize::PSGI 0/,
	},
	PREREQ_PM         => {
		qw/Carp        0
		   Encode      0
		   List::Util  0
		   POSIX       0
		   constant    0
		   feature     0
		   parent      0
		   strict      0
		   warnings    0

		   File::Slurp                      0
		   File::Which                      0
		   Gruntmaster::Data                0
		   HTML::Element::Library           0
		   HTML::TreeBuilder                0
		   JSON::MaybeXS                    0
		   Log::Log4perl                    0
		   PerlX::Maybe                     0
		   Plack::Builder                   0
		   Plack::Middleware::Auth::Complex 0
		   Plack::Util                      0
		   Scope::Upper                     0
		   Sort::ByExample                  0
		   Web::Simple                      0.019/,
	},
	META_MERGE        => {
		dynamic_config => 0,
		resources      => {
			repository => 'https://git.ieval.ro/?p=plack-app-gruntmaster.git',
		}
	}
)
