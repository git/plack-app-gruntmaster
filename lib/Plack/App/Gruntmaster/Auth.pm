package Plack::App::Gruntmaster::Auth;

use 5.014000;
use strict;
our $VERSION = '5999.000_001';

use parent qw/Plack::Middleware::Auth::Complex/;

sub call_register {
	my ($self, $req) = @_;
	return $self->bad_request('Parameter too long') if grep { length > 100 } $req->parameters->values;

	$self->SUPER::call_register($req);
}

sub create_user {
	my ($self, $parms) = @_;
	my %parms = $parms->flatten;
	my $sth = $self->{dbh}->prepare_cached('INSERT INTO users (id, name, email, phone, town, university, country, level, passphrase) VALUES (?,?,?,?,?,?,?,?,?)');
	$sth->execute(@parms{qw/username name email phone town university country level/}, $self->hash_passphrase($parms{password}));
}

1;
__END__
